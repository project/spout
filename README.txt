Spout module integrates the Spout (http://opensource.box.com/spout/) library with Drupal.

    Spout is a PHP library to read and write spreadsheet files (CSV, XLSX and ODS), in a fast and scalable way. Contrary to other file readers or writers, it is capable of processing very large files while keeping the memory usage really low (less than 3MB).

Supported Spreadsheet Types

Spout supports 3 types of spreadsheets: XLSX, ODS and CSV.
Spout provides a simple and unified API to read or create these different types of spreadsheets. Switching from one type to another is ridiculously easy!
Fast and Scalable

    Reading a small CSV file? No problem!
    Reading a huge XLSX file? No extra code needed!
    Writing an ODS file with millions of rows? Spout can do it in no time!

Why use Spout?

    Spout is capable of processing files of any size.
    Spout needs only 3MB of memory to process any file.
    Spout's streaming mechanism makes it incredibly fast.
    Spout's API is developer-friendly.

Requirements

    PHP version 5.4.0 or higher
    PHP extension php_zip enabled
    PHP extension php_xmlreader enabled

Dependencies

In order for this module to work, you must download the Spout library.

This module depends on the Libraries API (https://www.drupal.org/project/libraries) module.


Documentation: http://opensource.box.com/spout/docs/
Github:        https://github.com/box/spout
Gitter:        https://gitter.im/box/spout
